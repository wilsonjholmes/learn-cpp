#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);



/*
 * Complete the 'maxProfit' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER costPerCut
 *  2. INTEGER salePrice
 *  3. INTEGER_ARRAY lengths
 */

int maxProfit(int costPerCut, int salePrice, vector<int> lengths) {
    /* INPUT:
    STDIN     Function
    -----     -----
    1      →  costPerCut = 1
    10     →  salePrice = 10
    3      →  lengths[] size n = 3
    26     →  lengths = [26, 103, 59]
    103
    59

     OUTPUT:
     1770

     ---


     This problem I am thinking is a combination of finding the GCD while also ballancing the cost of the cuts to get to that gcd.
     so while I am looping through getting the gcd, I should also be calculating the cost of the cuts for that gcd on the largest number in the array.
    */
    // sort lengths
    sort(lengths.begin(), lengths.end());
    int maxProfit = 0;
    int length = lengths.size();
    int saleLen = 0;
    int totalUniformRods = 0;
    int numOfCuts = 0;
    int gcdTmp = lengths[0];
    // This part is O(n) by figuring out the best sale length in the same loop as the gcd
    for (int i = 1; i < length; i++) {
        if ((lengths[length-1]/gcdTmp)*costPerCut <= salePrice) { // this statement should be checked later if not getting the right result.
            gcdTmp = gcd(gcdTmp, lengths[i]);
        } else {
            saleLen = gcdTmp;
            break;
        }
    }
    // working too fast to think about complexity on this one.
    // how many saleLen's can be fit into the list? (totalUniformRods)
    int tmp;
    for (int i = 0; i < length; i++) {
        tmp = lengths[i]/saleLen;
        totalUniformRods += tmp;
        numOfCuts += tmp-1;
    }
    maxProfit = totalUniformRods*saleLen*salePrice-numOfCuts*costPerCut;
    cout << maxProfit << endl;
    return maxProfit;
}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string costPerCut_temp;
    getline(cin, costPerCut_temp);

    int costPerCut = stoi(ltrim(rtrim(costPerCut_temp)));

    string salePrice_temp;
    getline(cin, salePrice_temp);

    int salePrice = stoi(ltrim(rtrim(salePrice_temp)));

    string lengths_count_temp;
    getline(cin, lengths_count_temp);

    int lengths_count = stoi(ltrim(rtrim(lengths_count_temp)));

    vector<int> lengths(lengths_count);

    for (int i = 0; i < lengths_count; i++) {
        string lengths_item_temp;
        getline(cin, lengths_item_temp);

        int lengths_item = stoi(ltrim(rtrim(lengths_item_temp)));

        lengths[i] = lengths_item;
    }

    int result = maxProfit(costPerCut, salePrice, lengths);

    fout << result << "\n";

    fout.close();

    return 0;
}

string ltrim(const string &str) {
    string s(str);

    s.erase(
            s.begin(),
            find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace)))
    );

    return s;
}

string rtrim(const string &str) {
    string s(str);

    s.erase(
            find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
            s.end()
    );

    return s;
}
