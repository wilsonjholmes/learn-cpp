#include <iostream>
#include <vector>

using namespace std;
vector<string> split(const string &);


/*
 * Complete the 'lonelyinteger' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts INTEGER_ARRAY a as parameter.
 */

int lonelyinteger(vector<int> a) {
    // given vector a, return the only unique item in the vector
    int unique = 0;
    int length = a.size();
    for( int i = 0; i < length; i++ ) {
        unique ^= a[i];
    }
    return unique;
}

int main() {
    vector<int> arr;
    arr = {1,1,2};

    int sole_num = lonelyinteger(arr);
    cout << sole_num << endl;
}


