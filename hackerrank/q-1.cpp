#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);



/*
 * Complete the 'closestNumbers' function below.
 *
 * The function accepts INTEGER_ARRAY numbers as parameter.
 */

void closestNumbers(vector<int> numbers) {
    // sort the array O(n log n)
    sort(numbers.begin(), numbers.end());
    // loop through the array only once, for O(n) and save the pairs for the smallest difference in a vector
    vector<pair<int, int>> pairs;
    int smallestDiff = INT_MAX;
    for (int i = 0; i < numbers.size() - 1; i++) {
        int diff = numbers[i + 1] - numbers[i];
        if (diff < smallestDiff) {
            smallestDiff = diff;
            pairs.clear();
            pairs.push_back(make_pair(numbers[i], numbers[i + 1]));
        } else if (diff == smallestDiff) {
            pairs.push_back(make_pair(numbers[i], numbers[i + 1]));
        }
    }
    // print the pairs
    for (auto pair : pairs) {
        cout << pair.first << " " << pair.second << endl;
    }
}

int main()
{
    string numbers_count_temp;
    getline(cin, numbers_count_temp);

    int numbers_count = stoi(ltrim(rtrim(numbers_count_temp)));

    vector<int> numbers(numbers_count);

    for (int i = 0; i < numbers_count; i++) {
        string numbers_item_temp;
        getline(cin, numbers_item_temp);

        int numbers_item = stoi(ltrim(rtrim(numbers_item_temp)));

        numbers[i] = numbers_item;
    }

    closestNumbers(numbers);

    return 0;
}

string ltrim(const string &str) {
    string s(str);

    s.erase(
            s.begin(),
            find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace)))
    );

    return s;
}

string rtrim(const string &str) {
    string s(str);

    s.erase(
            find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
            s.end()
    );

    return s;
}
