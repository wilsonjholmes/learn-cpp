#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);
vector<string> split(const string &);

void printMatrix(vector<vector<int>> matrix) {
    cout << endl;
    int length = matrix.size();
    // loop through the matrix and print all elements
    for (int i = 0; i < length; i++) {
        int lengthi = matrix[i].size();
        for (int j = 0; j < lengthi; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}



/*
 * Complete the 'flippingMatrix' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts 2D_INTEGER_ARRAY matrix as parameter.
 */

int flippingMatrix(vector<vector<int>> matrix) {
    /*
     * - loop through the matrix and sum the left and right elements in each row.
     * - if the sum on the right is greater than the sum on the left, then we need to flip the row, otherwise we don't.
     * - do the same for the columns.
     * - sum up the elements in the submatrix that is at the top left corner of the matrix.
     */
    printMatrix(matrix);
    int sum = 0;
    int length = matrix.size();
    for (int i = 0; i < length; i++) {
        int left = 0;
        int right = 0;
        int lengthi = matrix[i].size();
        for (int j = 0; j < lengthi/2; j++) {
            left += matrix[i][j];
            right += matrix[i][lengthi/2];
        }
        cout << "L,R: " << left << " " << right;
        if (left < right) {
            cout << " <-- flip row " << i << endl;
            // make the right most element the left most element and so on for the rest of the elements.
            reverse(matrix[i].begin(), matrix[i].end());
        } else {
            cout << endl;
        }
    }
    cout << "look here: " << endl;
    printMatrix(matrix);
    for (int i = 0; i < length; i++) {
        int top = 0;
        int bottom = 0;
        // loop through the matrix and sum the top and bottom elements in each column.
        int lengthi = matrix[i].size();

        for (int j = 0; j < lengthi/2; j++) {
            top += matrix[j][i];
            cout << "!DEBUG: bottom -> matrix[" << j+lengthi/2 << "][" << i << "]: " << matrix[j+lengthi/2][i] << "" << endl;
            bottom += matrix[j+lengthi/2][i];
        }
        cout << "T,B: " << top << " " << bottom;
        if (top < bottom) {
            cout << " <-- flip col " << i << endl;
            // make the bottom most element the top most element and so on for the rest of the elements in that column only.
            for_each(matrix.begin(), matrix.end(), [&](vector<int> &row) {
                int lengthi = row.size();
                reverse(row.begin()+i, row.begin()+i+lengthi/2);
            });
        } else {
            cout << endl;
        }
    }

    printMatrix(matrix);
    // sum up the top left quadrant of the matrix
    for (int i = 0; i < length/2; i++) {
        int lengthi = matrix[i].size();
        for (int j = 0; j < lengthi/2; j++) {
            sum += matrix[i][j];
        }
    }
    return sum;
}

int main() {
    //414
    vector<vector<int>> matrix = {
                {112, 42, 83, 119},
                {56, 125, 56, 49},
                {15, 78, 101, 43},
                {62, 98, 114, 108}};
    int maxsum = flippingMatrix(matrix);
    cout << maxsum << endl;

    return 0;
}