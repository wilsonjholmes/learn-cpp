#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    // accept N, S, P and Q as space separated integers from stdin
    long int N, S, P, Q;
    cin >> N >> S >> P >> Q;

    /*
     * a[0] = S (modulo 2^31)
for i = 1 to N-1
    a[i] = a[i-1]*P+Q (modulo 2^31)
     * */

    vector<long int> a;
    a.push_back(S%(long int)pow(2,31));
    for (long int i = 1; i < N; i++) {
        a.push_back((a[i-1]*P+Q)%(long int)pow(2,31));
    }
    cout << a.size() << endl;
    return 0;
}