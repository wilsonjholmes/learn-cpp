#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);



/*
 * Complete the 'getMaxArea' function below.
 *
 * The function is expected to return a LONG_INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER w
 *  2. INTEGER h
 *  3. BOOLEAN_ARRAY isVertical
 *  4. INTEGER_ARRAY distance
 */

// Sample Input
/*
 * STDIN     Function
-----     --------
2     →   w = 2
2     →   h = 2
2     →   isVertical[] size n = 2
0     →   isVertical = [0, 1]
1
2     →   distance[] size n = 2
1     →   distance = [1, 1]
1

 * */

vector<long> getMaxArea(int w, int h, vector<bool> isVertical, vector<int> distance) {
    // returns an array where each element is the largest area available after adding the boundary i
    vector<long> maxArea;

    /*
    0 0 0
    0 0 0
    0 0 0

    1 2 2
    1 2 2
    1 2 2

    2 3 3
    3 4 4
    3 4 4
     * */

    // create a 2d array of size w * h
    vector<vector<int>> grid(w, vector<int>(h, 0));
    // initialize the grid with all 0s
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            grid[i][j] = 0;
        }
    }
    // for each boundary
    for (int i = 0; i < isVertical.size(); i++) {
        // if the boundary is vertical
        if (isVertical[i]) {
            // for each row
            for (int j = 0; j < h; j++) {
                // add the distance to the boundary
                grid[i][j] += distance[i];
            }
        } else {
            // for each column
            for (int j = 0; j < w; j++) {
                // add the distance to the boundary
                grid[j][i] += distance[i];
            }
        }
        // what is the largest repeated number in the grid?
        int max = 0;
        for (int j = 0; j < h; j++) {
            for (int k = 0; k < w; k++) {
                max = max > grid[k][j] ? max : grid[k][j];
            }
        }
        // add the max to the maxArea array
        maxArea.push_back((long)max);
    }

//    // vector to store the dimentions of each section after boundaries are added
//    vector<vector<long>> area;
//    for (int i = 0; i < distance.size(); i++) {
//        // vertical
//        if (isVertical[i]) {
//            area.push_back({distance[i], h});
//            long area1 = distance[i] * h;
//            area.push_back({w - distance[i], h});
//            long area2 = (w - distance[i]) * h;
//            maxArea.push_back(max(area1, area2));
//            // horizontal
//        } else {
//            area.push_back({w, distance[i]});
//            long area1 = w * distance[i];
//            area.push_back({w, h - distance[i]});
//            long area2 = w * (h - distance[i]);
//            maxArea.push_back(max(area1, area2));
//        }
//    }
    return maxArea;
}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string w_temp;
    getline(cin, w_temp);

    int w = stoi(ltrim(rtrim(w_temp)));

    string h_temp;
    getline(cin, h_temp);

    int h = stoi(ltrim(rtrim(h_temp)));

    string isVertical_count_temp;
    getline(cin, isVertical_count_temp);

    int isVertical_count = stoi(ltrim(rtrim(isVertical_count_temp)));

    vector<bool> isVertical(isVertical_count);

    for (int i = 0; i < isVertical_count; i++) {
        string isVertical_item_temp;
        getline(cin, isVertical_item_temp);

        bool isVertical_item = stoi(ltrim(rtrim(isVertical_item_temp))) != 0;

        isVertical[i] = isVertical_item;
    }

    string distance_count_temp;
    getline(cin, distance_count_temp);

    int distance_count = stoi(ltrim(rtrim(distance_count_temp)));

    vector<int> distance(distance_count);

    for (int i = 0; i < distance_count; i++) {
        string distance_item_temp;
        getline(cin, distance_item_temp);

        int distance_item = stoi(ltrim(rtrim(distance_item_temp)));

        distance[i] = distance_item;
    }

    vector<long> result = getMaxArea(w, h, isVertical, distance);

    for (int i = 0; i < result.size(); i++) {
        fout << result[i];

        if (i != result.size() - 1) {
            fout << "\n";
        }
    }

    fout << "\n";

    fout.close();

    return 0;
}

string ltrim(const string &str) {
    string s(str);

    s.erase(
            s.begin(),
            find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace)))
    );

    return s;
}

string rtrim(const string &str) {
    string s(str);

    s.erase(
            find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
            s.end()
    );

    return s;
}
