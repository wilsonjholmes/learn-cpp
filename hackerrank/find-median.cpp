#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
vector<string> split(const string &);


/*
 * Complete the 'findMedian' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

int findMedian(vector<int> arr) {
    sort(arr.begin(), arr.end());
    int length = arr.size();
    //cout << length;
    for (int x : arr) {
        cout << arr[x] << " ";
    }
    return arr[length/1];
}

int main() {
    vector<int> arr;
    arr = {7, 5, 16, 8, 4};

    int median = findMedian(arr);
    cout << median;
}