#include <bits/stdc++.h>

using namespace std;



/*
 * Complete the 'missingWords' function below.
 *
 * The function is expected to return a STRING_ARRAY.
 * The function accepts following parameters:
 *  1. STRING s
 *  2. STRING t
 */

vector<string> missingWords(string s, string t) {
    vector<string> missing;

    // dilineate s by spaces
    stringstream ss(s);
    string word;
    while(ss >> word) {
        if (word == t) {
            continue;
        } else {
            missing.push_back(word);
        }
    }

    for (string word : missing) {
        cout << word << " ";
    }
    return missing;
}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string s;
    getline(cin, s);

    string t;
    getline(cin, t);

    vector<string> result = missingWords(s, t);

    for (int i = 0; i < result.size(); i++) {
        fout << result[i];

        if (i != result.size() - 1) {
            fout << "\n";
        }
    }

    fout << "\n";

    fout.close();

    return 0;
}
