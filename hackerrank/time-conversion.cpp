#include <bits/stdc++.h>

using namespace std;

/*
 * Complete the 'timeConversion' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts STRING s as parameter.
 */

string timeConversion(string s) {
    string hr = s.substr(0,2);
    string hr_str = "";
    int hr_int = stoi(hr);
    int length = s.length();
    string min_sec = s.substr(2, length-4);
    //cout << min_sec;
    string dayornight = s.substr(length-2, length);
    //cout << dayornight;
    if (dayornight == "PM") {
        if (hr_int == 12) {
            hr_str = hr;
        } else {
            hr_int += 12;
            hr_str = to_string(hr_int);
        }

    } else if (dayornight == "AM") {
        if (hr_int == 12) {
            hr_str = "00";
        } else {
            hr_str = hr;
        }
    }
    cout << hr_str.append(min_sec);
    return hr_str;
}

int main()
{
    string s = "12:40:22AM";
    string result = timeConversion(s);
    return 0;
}
