#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);

/*
 * Complete the 'noPrefix' function below.
 *
 * The function accepts STRING_ARRAY words as parameter.
 */

void noPrefix(vector<string> words) {
    // create multiset of words to check
    multiset<string> words_to_check;
    // create set of prefixes we've already checked
    set<string> prefixes_checked;
    // loop through all the words
    for (string word : words) {
        string prefix = "";
        if (words_to_check.find(word) != words_to_check.end()) {
            cout << "BAD SET" << endl;
            cout << word << endl;
            return;
        }
        // // loop through each letter in the word, creating prefixes
        // for (char letter : word) {
        //     prefix += letter;
        //     prefixes_checked.insert(prefix);
        //     if (prefixes_checked.find(prefix) != prefixes_checked.end()) {
        //         cout << "BAD SET" << endl;
        //         cout << word << endl;
        //         return;
        //     }

        // }
        // words_to_check.insert(word);

        // check all 'prefixes' of current word in words_to_check set
        string prefixs = word;
        while (!prefixs.empty()) {
            // insert each prefix in list of prefixes to check future words against
            prefixes_checked.insert(prefixs);

            // check to see if the current prefix matches a word we've seen before
            if(words_to_check.find(prefixs) != words_to_check.end()) {
                // we found the prefix in the original word set
                cout << "BAD SET" << endl;
                cout << word << endl;
                return;
            } else {
                // remove one character from back of prefix, creating a new prefix
                prefixs.pop_back();
            }
        }
        // append the most recently checked word to my list of full words to check on future prefixes
        words_to_check.insert(word);
    }
    cout << "GOOD SET" << endl;
}

int main()
{
    string n_temp;
    getline(cin, n_temp);

    int n = stoi(ltrim(rtrim(n_temp)));

    vector<string> words(n);

    for (int i = 0; i < n; i++) {
        string words_item;
        getline(cin, words_item);

        words[i] = words_item;
    }

    noPrefix(words);

    return 0;
}

string ltrim(const string &str) {
    string s(str);

    s.erase(
            s.begin(),
            find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace)))
    );

    return s;
}

string rtrim(const string &str) {
    string s(str);

    s.erase(
            find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
            s.end()
    );

    return s;
}
