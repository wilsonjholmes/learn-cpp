#include <iostream>
#include <vector>

using namespace std;
vector<string> split(const string &);


/*
 * Complete the 'diagonalDifference' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts 2D_INTEGER_ARRAY arr as parameter.
 */

int diagonalDifference(vector<vector<int>> arr) {
    int l2r_dia = 0;
    int r2l_dia = 0;
    int length = arr.size();
    for (int i = 0; i < length; i++) {
        l2r_dia += arr[i][i];
        r2l_dia += arr[i][length - 1 - i];
    }
    int abs_diff = abs(l2r_dia - r2l_dia);
    return abs_diff;
}

int main() {
    vector<vector<int>> arr;
    arr = {{11,2,4}, {4,5,6}, {10,8,-12}};

    int dia_diff = diagonalDifference(arr);
    cout << dia_diff << endl;
}


