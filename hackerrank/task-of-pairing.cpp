#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);



/*
 * Complete the 'taskOfPairing' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts LONG_INTEGER_ARRAY freq as parameter.
 */

long taskOfPairing(vector<long> freq) {
    int long max_sold = 0;
    // 3 5 4 3 = 1s, 1-2, 2-2, 2-2, 3-3, 3-3, 4-4. = 7
    vector<long> weights;
    for (int i = 0; i < freq.size(); i++) {
        for (int j = 0; j < freq[i]; ++j) {
            weights.push_back(i + 1);
        }
    }
    // loop throught the weights vector
    for (int i = 0; i < weights.size(); i++) {
        // if the weights[i] and weights[i+1] are equal, or only one different, remove them from the vector and increment the max_sold
        if (weights[i] == weights[i + 1] || abs(weights[i] - weights[i + 1]) == 1) {
            max_sold++;
        }
    }
    // print out the weights vector
    for (int i = 0; i < weights.size(); i++) {
        cout << weights[i] << " ";
    }
    cout << "weights: " << weights.size() << endl;
    return max_sold;
}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string freq_count_temp;
    getline(cin, freq_count_temp);

    int freq_count = stoi(ltrim(rtrim(freq_count_temp)));

    vector<long> freq(freq_count);

    for (int i = 0; i < freq_count; i++) {
        string freq_item_temp;
        getline(cin, freq_item_temp);

        long freq_item = stol(ltrim(rtrim(freq_item_temp)));

        freq[i] = freq_item;
    }

    long result = taskOfPairing(freq);

    fout << result << "\n";

    fout.close();

    return 0;
}

string ltrim(const string &str) {
    string s(str);

    s.erase(
            s.begin(),
            find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace)))
    );

    return s;
}

string rtrim(const string &str) {
    string s(str);

    s.erase(
            find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
            s.end()
    );

    return s;
}


