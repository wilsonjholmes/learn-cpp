#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);



/*
 * Complete the 'palindromeIndex' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 */

int palindromeIndex(string s) {
    string Str = s;
    reverse(Str.begin(), Str.end());
    if (s == Str) {
        return -1;
    }
    //s = abcbda
    //S = adbcba

    // adbcba
    // abcdba
    // loop through the string and if the 0+i and n-i are not equal return the index check i+1 to see which index is the problem
    int lengthh = s.length();
    for (int i = 0; i < lengthh; i++) {
        if (s[i] != Str[i]) {
            if (s[i] == Str[i + 1]) {
                return lengthh - i;
            } else if (s[i + 1] == Str[i]) {
                return i + 1;
            }
        }
    }



}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string q_temp;
    getline(cin, q_temp);

    int q = stoi(ltrim(rtrim(q_temp)));

    for (int q_itr = 0; q_itr < q; q_itr++) {
        string s;
        getline(cin, s);

        int result = palindromeIndex(s);

        fout << result << "\n";
    }

    fout.close();

    return 0;
}

string ltrim(const string &str) {
    string s(str);

    s.erase(
            s.begin(),
            find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace)))
    );

    return s;
}

string rtrim(const string &str) {
    string s(str);

    s.erase(
            find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
            s.end()
    );

    return s;
}
