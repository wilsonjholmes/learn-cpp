#include <bits/stdc++.h>

using namespace std;

/*
 * Complete the 'countingSort' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

vector<int> countingSort(vector<int> arr) {
   /*
    * - loop through vector and count the number of occurences of each unique element
    * - create a new int vector of size of the largest element in the vector
    * - loop through the new vector and fill it with the number of occurences of each unique element using the index
    *   as the value of the element
    * */
    int max = *max_element(arr.begin(), arr.end());
    vector<int> count(max + 1, 0);
    int length = 100;
    for (int i = 0; i < length; i++) {
        count[arr[i]]++;
    }
    return count;
}

int main() {
    vector<int> arr;
    arr = {63,54,17,78,43,70,32,97,16,94,74,18,60,61,35,83,13,56,75,52,70,12,24,37,17,0,16,64,34,81,82,24,69,2,30,61,83,37,97,16,70,53,0,61,12,17,97,67,33,30,49,70,11,40,67,94,84,60,35,58,19,81,16,14,68,46,42,81,75,87,13,84,33,34,14,96,7,59,17,98,79,47,71,75,8,27,73,66,64,12,29,35,80,78,80,6,5,24,49,82};
    vector<int> freq_arr = countingSort(arr);
    int length = freq_arr.size();
    for (int i = 0; i < length; i++) {
        cout << freq_arr[i] << endl;
    }
}


