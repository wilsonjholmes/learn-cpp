#include <iostream>
#include <string>
#include <memory>

using namespace std;

class Human {
    float dna;

    // Constructor
    Human() {
        dna = 0.0;

    }

    // Destructor
    ~Human() {

    }


    public:
        string face;
        // This can be defined outside of the class...
        // void walk() {
        //    cout << "I am walking";
        // }

        // You can redefine functions over themselves by overloading.
        void walk(int speed) {
            cout << "I am walking at " << speed << " mph";
        }
        void walk(float speed) {
            cout << "I am walking at " << speed << " mph";
        }
};

// They can be defined inside of the class as mentioned above.
// void Human::walk() {
//     cout << "I am walking";
// }

class SuperHero: public Human {
    public:
        string name;
        string power = "flying";
        void powers() {
            cout << "I am good at" << power;
        }
};

int main() {
    // Get rid of this by using a namespace.
    // std::cout << "Hello, World!" << endl;
    cout << "Hello, World!";
    // Use the string type instead of char.
    // char hello[] = "Hey Ma look! Static Typing!";
    string hello = "Hey Ma look! Static Typing!";

    // pointers can be done the C way...
    // Human wilson;
    // Human* myPtr = new Human;
    // delete myPtr;
    
    // ... But smart pointers are cool
    //unique_ptr<Human> wilson(new Human);
    //cout << wilson->dna;

    return 0;
}
